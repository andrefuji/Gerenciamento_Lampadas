/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Pichau
 */
@Entity
@Table(name = "genio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Genio.findAll", query = "SELECT g FROM Genio g")
    , @NamedQuery(name = "Genio.findByIdGenio", query = "SELECT g FROM Genio g WHERE g.idGenio = :idGenio")
    , @NamedQuery(name = "Genio.findByNome", query = "SELECT g FROM Genio g WHERE g.nome = :nome")
    , @NamedQuery(name = "Genio.findByHumor", query = "SELECT g FROM Genio g WHERE g.humor = :humor")})
public class Genio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idGenio")
    private Integer idGenio;
    @Basic(optional = false)
    @Column(name = "Nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "Humor")
    private String humor;
    @JoinColumn(name = "Fada_idFada", referencedColumnName = "idFada")
    @ManyToOne
    private Fada fadaidFada;
    @OneToMany(mappedBy = "genioidGenio")
    private Collection<Lampada> lampadaCollection;

    public Genio() {
    }

    public Genio(Integer idGenio) {
        this.idGenio = idGenio;
    }

    public Genio(Integer idGenio, String nome, String humor) {
        this.idGenio = idGenio;
        this.nome = nome;
        this.humor = humor;
    }

    public Integer getIdGenio() {
        return idGenio;
    }

    public void setIdGenio(Integer idGenio) {
        this.idGenio = idGenio;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getHumor() {
        return humor;
    }

    public void setHumor(String humor) {
        this.humor = humor;
    }

    public Fada getFadaidFada() {
        return fadaidFada;
    }

    public void setFadaidFada(Fada fadaidFada) {
        this.fadaidFada = fadaidFada;
    }

    @XmlTransient
    public Collection<Lampada> getLampadaCollection() {
        return lampadaCollection;
    }

    public void setLampadaCollection(Collection<Lampada> lampadaCollection) {
        this.lampadaCollection = lampadaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGenio != null ? idGenio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Genio)) {
            return false;
        }
        Genio other = (Genio) object;
        if ((this.idGenio == null && other.idGenio != null) || (this.idGenio != null && !this.idGenio.equals(other.idGenio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Genio[ idGenio=" + idGenio + " ]";
    }
    
}
