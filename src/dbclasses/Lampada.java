/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pichau
 */
@Entity
@Table(name = "lampada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lampada.findAll", query = "SELECT l FROM Lampada l")
    , @NamedQuery(name = "Lampada.findByIdLampada", query = "SELECT l FROM Lampada l WHERE l.idLampada = :idLampada")
    , @NamedQuery(name = "Lampada.findByTipo", query = "SELECT l FROM Lampada l WHERE l.tipo = :tipo")
    , @NamedQuery(name = "Lampada.findByHabitada", query = "SELECT l FROM Lampada l WHERE l.habitada = :habitada")
    , @NamedQuery(name = "Lampada.findByUtilizada", query = "SELECT l FROM Lampada l WHERE l.utilizada = :utilizada")})
public class Lampada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idLampada")
    private Integer idLampada;
    @Basic(optional = false)
    @Column(name = "Tipo")
    private String tipo;
    @Basic(optional = false)
    @Column(name = "Habitada")
    private boolean habitada;
    @Basic(optional = false)
    @Column(name = "Utilizada")
    private boolean utilizada;
    @JoinColumn(name = "Genio_idGenio", referencedColumnName = "idGenio")
    @ManyToOne
    private Genio genioidGenio;

    public Lampada() {
    }

    public Lampada(Integer idLampada) {
        this.idLampada = idLampada;
    }

    public Lampada(Integer idLampada, String tipo, boolean habitada, boolean utilizada) {
        this.idLampada = idLampada;
        this.tipo = tipo;
        this.habitada = habitada;
        this.utilizada = utilizada;
    }

    public Integer getIdLampada() {
        return idLampada;
    }

    public void setIdLampada(Integer idLampada) {
        this.idLampada = idLampada;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean getHabitada() {
        return habitada;
    }

    public void setHabitada(boolean habitada) {
        this.habitada = habitada;
    }

    public boolean getUtilizada() {
        return utilizada;
    }

    public void setUtilizada(boolean utilizada) {
        this.utilizada = utilizada;
    }

    public Genio getGenioidGenio() {
        return genioidGenio;
    }

    public void setGenioidGenio(Genio genioidGenio) {
        this.genioidGenio = genioidGenio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLampada != null ? idLampada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lampada)) {
            return false;
        }
        Lampada other = (Lampada) object;
        if ((this.idLampada == null && other.idLampada != null) || (this.idLampada != null && !this.idLampada.equals(other.idLampada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Lampada[ idLampada=" + idLampada + " ]";
    }
    
}
