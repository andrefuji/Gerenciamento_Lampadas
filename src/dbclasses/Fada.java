/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbclasses;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Pichau
 */
@Entity
@Table(name = "fada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fada.findAll", query = "SELECT f FROM Fada f")
    , @NamedQuery(name = "Fada.findByIdFada", query = "SELECT f FROM Fada f WHERE f.idFada = :idFada")
    , @NamedQuery(name = "Fada.findByNome", query = "SELECT f FROM Fada f WHERE f.nome = :nome")
    , @NamedQuery(name = "Fada.findByUtilizada", query = "SELECT f FROM Fada f WHERE f.utilizada = :utilizada")})
public class Fada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idFada")
    private Integer idFada;
    @Column(name = "Nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "Utilizada")
    private boolean utilizada;
    @JoinColumn(name = "Usuario_idUsuario", referencedColumnName = "idUsuario")
    @ManyToOne
    private Usuario usuarioidUsuario;
    @OneToMany(mappedBy = "fadaidFada")
    private Collection<Genio> genioCollection;

    public Fada() {
    }

    public Fada(Integer idFada) {
        this.idFada = idFada;
    }

    public Fada(Integer idFada, boolean utilizada) {
        this.idFada = idFada;
        this.utilizada = utilizada;
    }

    public Integer getIdFada() {
        return idFada;
    }

    public void setIdFada(Integer idFada) {
        this.idFada = idFada;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean getUtilizada() {
        return utilizada;
    }

    public void setUtilizada(boolean utilizada) {
        this.utilizada = utilizada;
    }

    public Usuario getUsuarioidUsuario() {
        return usuarioidUsuario;
    }

    public void setUsuarioidUsuario(Usuario usuarioidUsuario) {
        this.usuarioidUsuario = usuarioidUsuario;
    }

    @XmlTransient
    public Collection<Genio> getGenioCollection() {
        return genioCollection;
    }

    public void setGenioCollection(Collection<Genio> genioCollection) {
        this.genioCollection = genioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFada != null ? idFada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fada)) {
            return false;
        }
        Fada other = (Fada) object;
        if ((this.idFada == null && other.idFada != null) || (this.idFada != null && !this.idFada.equals(other.idFada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dbclasses.Fada[ idFada=" + idFada + " ]";
    }
    
}
