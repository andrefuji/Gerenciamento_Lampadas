/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetolp2;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author Leonardo
 */
public class MySqlConnection {

    String ip, porta, banco, user, pass;

    public MySqlConnection(String ip, String porta, String banco, String user, String pass) {
        this.ip = ip;
        this.porta = porta;
        this.banco = banco;
        this.user = user;
        this.pass = pass;
    }

    public TableModel GetDataToTableModel(String query) throws SQLException { // menampilkan data dari database
        Connection mySqlConnector = null;
        TableModel result = null;
        try {
            String DB = "jdbc:mysql://" + ip + ":3306/" + banco;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            mySqlConnector = (Connection) DriverManager.getConnection(DB, user, pass);
            //System.out.println(mySqlConnector);
            try {
                java.sql.Statement stm = mySqlConnector.createStatement();
                java.sql.ResultSet sql = stm.executeQuery(query);
                result = DbUtils.resultSetToTableModel(sql);
                mySqlConnector.close();
                return result;
            } catch (SQLException e) {
            }

        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "erro: " + e);
        }
        mySqlConnector.close();
        return null;
    }

    public String GetDataToString(String query) throws SQLException { // menampilkan data dari database

        Connection mySqlConnector = null;
        try {
            String DB = "jdbc:mysql://" + ip + ":3306/" + banco;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            mySqlConnector = (Connection) DriverManager.getConnection(DB, user, pass);

            try {
                java.sql.Statement stm = mySqlConnector.createStatement();
                java.sql.ResultSet sql = stm.executeQuery(query);
                if (!sql.next()) {
                    mySqlConnector.close();
                    return "";
                }
                sql = stm.executeQuery(query);
                String result = DbUtils.resultSetToNestedList(sql).
                        get(0).toString().replace("[", "").replace("]", "");
                if (result.equals("null")) {
                    mySqlConnector.close();
                    return "";
                } else {
                    mySqlConnector.close();
                    return result;
                }
            } catch (SQLException e) {
                mySqlConnector.close();
                return "";
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "erro: " + e);
        }
        mySqlConnector.close();
        return "";
    }

}
